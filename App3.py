
from flask import Flask
from flask import jsonify

from flask import Response
from flask import request,json

from flask_sqlalchemy import SQLAlchemy


exp_mgr_app = Flask(__name__)

exp_mgr_app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/mydb3'
expnsdb = SQLAlchemy(exp_mgr_app);

@exp_mgr_app.route('/')
def deafault():
	return "Welcome"



@exp_mgr_app.route('/v1/expenses/', methods=['POST'])
def saveexpense2():
	CreateDatabase()
	expnsdb.create_all()

	temp_expns = request.get_json(force = True )

	id = temp_expns['id'] # extract id from the request
	name = temp_expns['name']
	email =  temp_expns['email']
	category = temp_expns['category']
	description = temp_expns['description']
	link = temp_expns['link']
	estimated_costs = temp_expns['estimated_costs']
	submit_date = temp_expns['submit_date']
	status = "pending"
	decision_date = ""

	temp_expns = Expense(id,name, email, category, description, link, estimated_costs, submit_date, status, decision_date)
	expnsdb.session.add(temp_expns)
	expnsdb.session.commit()

	res = { 
	"id":temp_expns.id,
	"name":temp_expns.name,
	"email":temp_expns.email,
	"category":temp_expns.category,
	"description":temp_expns.description,
	"link":temp_expns.link,
	"estimated_costs":temp_expns.estimated_costs,
	"submit_date":temp_expns.submit_date,
	"status":temp_expns.status,
	"decision_date":temp_expns.decision_date
	}

	jres=Response(response=json.dumps(res),status=201,mimetype="application/json")
	return jres


@exp_mgr_app.route('/v1/expenses/<int:expense_id>', methods=['GET'])
def getexpense(expense_id):
	if Expense.query.filter_by(id=expense_id).first() is not None:
		temp_expns = Expense.query.filter_by(id=expense_id).first()
		res = { 
		"id":temp_expns.id,
		"name":temp_expns.name,
		"email":temp_expns.email,
		"category":temp_expns.category,
		"description":temp_expns.description,
		"link":temp_expns.link,
		"estimated_costs":temp_expns.estimated_costs,
		"submit_date":temp_expns.submit_date,
		"status":temp_expns.status,
		"decision_date":temp_expns.decision_date
		}
		jres = jsonify(res)
		jres.status_code = 200
		return jres
	else:
		jres = Response(status = 404)
		return jres

@exp_mgr_app.route('/v1/expenses/<int:expense_id>',methods=['PUT'])
def updateexpense(expense_id):

	expns = request.get_json(force = True)
	if Expense.query.filter_by(id=expense_id).first() is not None:
		temp_expns = Expense.query.filter_by(id=expense_id).first()
		temp_expns.estimated_costs = expns['estimated_costs']
		expnsdb.session.commit()
		res = Response(status = 202)
		return res
	else:
		jres = Response(status = 404)
		return jres

@exp_mgr_app.route('/v1/expenses/<int:expense_id>', methods=['DELETE'])
def deleteexpense(expense_id):
	if Expense.query.filter_by(id=expense_id).first() is not None:
		temp_expns = Expense.query.filter_by(id=expense_id).first()
		expnsdb.session.delete(temp_expns)
		expnsdb.session.commit()
		res = Response(status = 204)
		return res
	else:
		jres = Response(status = 404)
		return jres



class Expense(expnsdb.Model):
	

	id = expnsdb.Column(expnsdb.Integer, primary_key = True)
	name = expnsdb.Column(expnsdb.String(80))
	email = expnsdb.Column(expnsdb.String(80))
	category = expnsdb.Column(expnsdb.String(80))
	description = expnsdb.Column(expnsdb.String(100))
	link = expnsdb.Column(expnsdb.String(100))
	estimated_costs = expnsdb.Column(expnsdb.String(40))
	submit_date = expnsdb.Column(expnsdb.String(40))
	status = expnsdb.Column(expnsdb.String(80))
	decision_date = expnsdb.Column(expnsdb.String(40))


	def __init__(self, id, name, email, category, description, link, estimated_costs, submit_date, status="pending", decision_date=""):
    		self.id = id
		self.name = name
		self.email = email
		self.category = category
		self.description = description
		self.link = link
		self.estimated_costs = estimated_costs
		self.submit_date = submit_date
		self.status = status
		self.decision_date = decision_date

class CreateDatabase():
	def __init__(self):
		import sqlalchemy
		engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%("root","root","127.0.0.1"))
		engine.execute("CREATE DATABASE IF NOT EXISTS %s "%("mydb3"))

if __name__ == '__main__':
	exp_mgr_app.debug = True
	exp_mgr_app.run(host='127.0.0.1',port=5003)




