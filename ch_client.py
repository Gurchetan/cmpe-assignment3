

from flask import Flask, request, abort,jsonify
from consistenthashing import ConsistentHashing
import simplejson as json
import requests

app = Flask(__name__)
ConsistentHashing = ConsistentHashing(False)

# adding all nodes for future mapping
ConsistentHashing.addnode("1", "127.0.0.1:5001")
ConsistentHashing.addnode("2", "127.0.0.1:5002")
ConsistentHashing.addnode("3", "127.0.0.1:5003")

# default message
@app.route('/')
def index():
    return "Consistent Hashing Assignment"

# function to handle POST request with - version 1
@app.route('/v1/expenses/<expense_id>', methods=['POST'])
def handlepost(expense_id):
    rec = json.loads(request.data)
    # if not content or not 'name' in content:
    #     abort(404)
    s = ConsistentHashing.getnode(str(expense_id))
    req = requests.post("http://"+s+"/v1/expenses/"+expense_id, data=json.dumps(rec))
    return "Record saved by " + s+ "\n" +req.text

# function to handle POST request with - version 2 - handle id sent as a part of request
@app.route('/v1/expenses', methods=['POST'])
def handlepost2():
    rec = json.loads(request.data)
    if not rec or not 'name' in rec:
        abort(404)
    s = ConsistentHashing.getnode(str(rec['id']))
    r = requests.post("http://"+s+"/v1/expenses/", data=json.dumps(rec))
    return "Record saved by " + s+ "\n" +r.text

# function to handle the GET requests
@app.route('/v1/expenses/<expense_id>', methods=['GET'])
def handleget(expense_id):
    s = ConsistentHashing.getnode(str(expense_id))
    res = requests.get("http://"+s+"/v1/expenses/"+expense_id)
    return "Received record from " + s + "\n" + res.text

# run app service
if __name__ == "__main__":
	app.run(host="127.0.0.1", port=4000, debug=True)
