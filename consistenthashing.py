

import bisect
from hashlib import md5

class ConsistentHashing(object):
    
    def __init__(self, debug=False):
        self._numofnodes = 0
        self._keys = []
        self._nodes = {}
        self._debug = debug

    def getHash(self, key):
        hashvalue  = long(md5(key).hexdigest(), 16)
        if(self._debug):
            print 'in getHash / key: ', key, " hashvalue: ", hashvalue
        return hashvalue

    def addnode(self, nodename, node):
        nodehash = self.getHash(nodename)
        if nodehash in self._nodes:
            raise ValueError("Node name %r is "
                "already present" % nodename)
        self._nodes[nodehash] = node
        bisect.insort(self._keys, nodehash)
        self._numofnodes += 1
        if(self._debug):
            print 'in addnode / node:', nodename, " node:", node, " nodehash:", nodehash, " self_keys: ", self._keys, " self_nodes: ", self._nodes
    
    def removenode(self, nodename):
        nodehash = self.getHash(nodename)
        if nodehash not in self._nodes:
            raise ValueError("Node name %r is "
                "not present" % nodename)
        del self._nodes[nodehash]
        position = bisect.bisect_left(self._keys, nodehash)
        del self._keys[position]
        self._numofnodes -= 1

        if(self._debug):
            print 'in removenode / node:', nodename, " nodehash:", nodehash, " self_keys: ", self._keys, " self_nodes: ", self._nodes

    def getnode(self, key):
        nodehash = self.getHash(key)
        start = bisect.bisect(self._keys, nodehash)
        if start == len(self._keys):
            start = 0

        if(self._debug):
            print 'in getnode / key:', key, " nodehash:", nodehash, " self._nodes[self._keys[start]]: ", self._nodes[self._keys[start]]

        return self._nodes[self._keys[start]]
