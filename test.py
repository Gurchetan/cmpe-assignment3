# Test Client
# @author - Nitinkumar Gove
# Version - 3.0
from consistenthashing import ConsistentHashing
import requests

consistentHashing = ConsistentHashing(False)
consistentHashing.addnode("1", "127.0.0.1:5001")
consistentHashing.addnode("2", "127.0.0.1:5002")
consistentHashing.addnode("3", "127.0.0.1:5003")

dict = {}

for i in xrange(1,11):
    node = consistentHashing.getnode(str(i))
    r = requests.get("http://127.0.0.1:4000/v1/expenses/"+str(i))
    print r.text

    server = consistentHashing.getnode(str(i))
    if server not in dict:
        dict[server] = 0
    dict[server] += 1

print "TEST SUMMARY"
print dict